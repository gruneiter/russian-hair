const path = require('path');
import webpack from 'webpack';

module.exports = {
  mode: 'development',
  entry: {
    main:'./src/scripts/script.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: 'script.js'
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "eslint-loader"
      },
    ],
  }
}
