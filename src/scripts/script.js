import $ from 'jquery';
import fancybox from '@fancyapps/fancybox';
import flexslider from 'flexslider';
import headerMobileMenu from '../blocks/header-mobile/header-mobile-menu/header-mobile-menu.js';
import homeTopPar from '../blocks/home-top/home-top.js';
import pricesSwitch from '../blocks/price-block/price-block.js';
import colorsSlider from '../blocks/colors-slider/colors-slider.js';

headerMobileMenu();
homeTopPar();
pricesSwitch();
colorsSlider();
